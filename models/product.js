// const fs = require("fs");
// const path = require("path");
// const rootDir = require("../utils/path");
// const Cart = require('./cart')
// const p = path.join(rootDir, "data", "products.json");

// const getProductsFromFile = cb => {
//     fs.readFile(p, (err, fileContent) => {
//       if (err) {
//         cb([]);
//       } else {
//         cb(JSON.parse(fileContent));
//       }
//     });
//   };
// module.exports = class Product {
//   constructor(id, title, imageUrl, description, price) {
//     this.id = id;
//     this.title = title;
//     this.imageUrl = imageUrl;
//     this.description = description;
//     this.price = price;

//   }
//   // save() {
//   //   this.id = Math.random().toString();
//   //   // fs.readFile(p, (err, fileContent) => {
//   //   //   let products = [];

//   //   //   if (!err) {
//   //   //     products = JSON.parse(fileContent);
//   //   //   }
//   //   //   products.push(this);
//   //   //   fs.writeFile(p, JSON.stringify(products), (err) => {
//   //   //     console.log(err);
//   //   //   });
//   //   // });
//   //   getProductsFromFile(products => {
//   //       products.push(this);
//   //       fs.writeFile(p, JSON.stringify(products), err => {
//   //         console.log(err);
//   //       });
//   //     });
//   // }
//   save() {
//     getProductsFromFile(products => {
//       if (this.id) {
//         const existingProductIndex = products.findIndex(
//           prod => prod.id === this.id
//         );
//         const updatedProducts = [...products];
//         updatedProducts[existingProductIndex] = this;
//         fs.writeFile(p, JSON.stringify(updatedProducts), err => {
//           console.log(err);
//         });
//       } else {
//         this.id = Math.random().toString();
//         products.push(this);
//         fs.writeFile(p, JSON.stringify(products), err => {
//           console.log(err);
//         });
//       }
//     });
//   }

//   static deleteById(id){
//     getProductsFromFile(products => {
//       const product = products.find(prod => prod.id === id)
//       const updateProducts = products.filter(p => p.id !== id)
//       fs.writeFile(p, JSON.stringify(updateProducts), err => {
//         if(!err){
//           Cart.deleteProduct(id, product.price)
//         }
//       })
//     })
//   }

//   static fetchAll(cb) {
//     // const p = path.join(rootDir, "data", "products.json");
//     // fs.readFile(p,(err,fileContent) => {
//     //     if(err){
//     //         return []
//     //     }
//     //     return JSON.parse(fileContent)
//     // })
//     getProductsFromFile(cb);
//   }
//   static findById(id,cb){
//       getProductsFromFile(products => {
//         const product = products.find(p => p.id === id)
//         cb(product)
//       })
//   }
// };

//connect with mysql simple
// const fs = require("fs");
// const path = require("path");
// const rootDir = require("../utils/path");
// const Cart = require('./cart')
// const p = path.join(rootDir, "data", "products.json");
// const db = require('../utils/database')

// module.exports = class Product {
//   constructor(id, title, imageUrl, description, price) {
//     this.id = id;
//     this.title = title;
//     this.imageUrl = imageUrl;
//     this.description = description;
//     this.price = price;

//   }

//   save() {
//     return db.execute("INSERT INTO products (title,price,imageUrl,description) values (?,?,?,?)",
//     [this.title, this.price, this.imageUrl, this.description])
//   }

//   static deleteById(id){

//   }

//   static fetchAll() {
//    return db.execute("SELECT * FROM products")
//   }

//   static findById(id){
//     return db.execute("SELECT * FROM products WHERE products.id = ?", [id])
//   }
// };

//connect with sequelize method
// const Sequelize = require('sequelize')

// const sequelize = require('../utils/database')

// const Product = sequelize.define('product',{
//   id:{
//     type:Sequelize.INTEGER,
//     autoIncrement: true,
//     allowNull: false,
//     primaryKey: true
//   },
//   title:{
//     type:Sequelize.STRING,
//   },
//   price:{
//     type:Sequelize.DOUBLE,
//     allowNull: false
//   },
//   imageUrl:{
//     type:Sequelize.STRING,
//     allowNull: false
//   },
//   description:{
//     type:Sequelize.STRING,
//     allowNull:false,
//   }
// })

// module.exports = Product;

// connect with mongodb
const mongodb = require("mongodb");
const getDb = require("../utils/database").getDb;

class Product {
  constructor(title, price, description, imageUrl, id, userId) {
    this.title = title;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
    this._id = id ? new mongodb.ObjectId(id) : null;
    this.userId = userId
  }
  save() {
    const db = getDb();
    let dbOp;
    if (this._id) {
      //update product
      dbOp = db
        .collection("products")
        .updateOne({ _id: this._id }, { $set: this });
    } else {
      dbOp = db.collection("products").insertOne(this);
    }

    return dbOp
      .then((result) => console.log("result", result))
      .catch((err) => console.log(err));
  }
  static fetchAll() {
    const db = getDb();
    return db
      .collection("products")
      .find()
      .toArray()
      .then((products) => {
        return products;
      })
      .catch((err) => console.log(err));
  }
  static findById(prodId) {
    const db = getDb();
    return db
      .collection("products")
      .find({ _id: new mongodb.ObjectId(prodId) })
      .next()
      .then((product) => {
        return product;
      })
      .catch((err) => console.log(err));
  }
  static deleteById(prodId) {
    const db = getDb();
    return db
      .collection("products")
      .deleteOne({ _id: new mongodb.ObjectId(prodId) })
      .then((product) => {
        return product;
      })
      .catch((err) => console.log(err));
  }
}
module.exports = Product;
