// const http = require("http");
// const fs = require("fs");

// // function rqListener(req,res){}
// // http.createServer(rqListener())

// const server = http.createServer((req, res) => {
//   console.log(req.url, req.method, req.headers);
//   if (req.url === "/") {
//     res.write("<html>");
//     res.write("<head><title>Enter message</title></head>");
//     res.write(
//       "<body><form action='/message' method='POST'><input type='text' name='message'><button type='submit'>Post</button></form></body>"
//     );
//     res.write("</html>");
//     return res.end();
//   }
//   if (req.url === "/message" && req.method === "POST") {
//     const body = [];
//     req.on("data", (chunk) => {
//       console.log(chunk);
//       body.push(chunk);
//     });
//     return req.on("end", () => {
//       const parsedBody = Buffer.concat(body).toString();

//       const message = parsedBody.split("=")[1];
//       // fs.writeFileSync("message.txt", message);
//       fs.writeFile("message.txt", message, (err) => {
//         res.statusCode = 302;
//         res.setHeader("Location", "/");
//         return res.end();
//       });
//     });
//   }
//   res.setHeader("Content-Type", "text/html");
//   res.write("<html>");
//   res.write("<head><title>My page title</title></head>");
//   res.write("<body><h1>hello from node js</h1></body>");
//   res.write("</html>");
//   res.end();
// });

// server.listen(3001);

// const http = require('http')
// const routes = require('./routes');
// const server = http.createServer(routes.handler)
// // console.log(routes.someText)
// server.listen(3001)


// sequelize method
// const http = require("http");
// const express = require("express");
// const path = require("path");
// const bodyParser = require("body-parser");
// const adminRoutes = require("./routes/admin");
// const shopRoutes = require("./routes/shop");
// const errorController = require("./controllers/error");
// const app = express();
// // const db = require("./utils/database");
// const sequelize = require("./utils/database");

// const Product = require("./models/product");
// const User = require("./models/user");
// const Cart = require("./models/cart");
// const Order = require("./models/order");
// const CartItem = require("./models/cart-item");
// const OrderItem = require("./models/order-item");

// // if database hase createPool then
// // db.execute("SELECT * FROM products")
// //   .then()
// //   .catch((err) => console.log(err));

// // if database has createConnection then
// // db.connect((err) => {
// //   if (err) { return console.log(err)};
// //   console.log("Connected to MySQL database!");
// // })
// // db.query('SELECT * FROM products', (err, rows) => {
// //   if (err) return console.log(err);
// //   console.log('Data received from MySQL:');
// //   console.log(rows);
// // });

// app.set("view engine", "ejs");
// app.set("views", "views");
// // app.use((req,res,next) => {
// //     console.log("In the middleware!")
// //     next() // allow the request to continue next middleware in line
// // });
// // app.use((req,res,next) => {
// //     console.log("In other middleware!")
// //     res.send('<h1>hello from express</h1>')
// // });
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(express.static(path.join(__dirname, "public")));

// app.use((req, res, next) => {
//   User.findByPk(1)
//     .then((user) => {
//       req.user = user;
//       next();
//     })
//     .catch((err) => console.log(err));
// });
// app.use(adminRoutes);
// app.use(shopRoutes);

// app.use(errorController.get404page);

// app.get("/", (req, res) => {
//   console.log(req.user);
// });

// // relationship of model
// Product.belongsTo(User, { constraints: true, onDelete: "CASCADE" });
// User.hasMany(Product);
// User.hasOne(Cart);
// Cart.belongsTo(User);
// Cart.belongsToMany(Product, { through: CartItem });
// Product.belongsToMany(Cart, { through: CartItem });
// Order.belongsTo(User);
// User.hasMany(Order);
// Order.belongsToMany(Product, { through: OrderItem });

// sequelize
//   .sync()
//   .then((result) => {
//     return User.findByPk(1);
//     // console.log(result)
//   })
//   .then((user) => {
//     if (!user) {
//       return User.create({ id: 1, name: "test", email: "test@gmail.com" });
//     }
//     return user;
//   })
//   .then((user) => {
//    return user.createCart()
//   })
//   .then((user) => {
//     // console.log(user);
//     app.listen(3001);
//   })
//   .catch((err) => console.log(err));

// // app.listen(3001);


// mongodb setup
const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const User = require("./models/user");
const errorController = require('./controllers/error');
const db = require('./utils/database');

db.mongoConnect(() => {
  app.listen(3001);
});

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  User.findById("6406e9399ae59fea48944439").then(user => {
    req.user = new User(user.name, user.email, user.cart, user._id);
    next();
  }).catch(err => console.log(err))
});

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404page);
