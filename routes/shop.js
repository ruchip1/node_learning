const express = require('express')
const path = require('path')
const rootDir = require('../utils/path')
const router = express.Router()
const adminData = require('./admin')
const shopController = require('../controllers/shop');
// router.get('/', (req,res,next) => {
//     console.log("In other middleware!")
//     // res.send('<h1>hello from express</h1>')
//     res.sendFile(path.join(rootDir,'views','shop.html'))
// });
// router.get('/', (req,res,next) => {
//     console.log(adminData.products)
//     // res.send('<h1>hello from express</h1>')
//     res.sendFile(path.join(rootDir,'views','shop.html'))
// });
router.get('/', shopController.getIndex);
router.get('/products', shopController.getProducts);

router.get('/products/:productId', shopController.getProduct);

// router.get('/cart', shopController.getCart);

// router.post('/cart', shopController.postCart)

// router.get('/orders',shopController.getOrders)

// router.post('/cart-delete-item',shopController.postCartDelete)

// router.post('/create-order',shopController.postOrder)

// router.get('/checkout', shopController.getCheckout)

module.exports = router;