const Product = require("../models/product");

exports.getAddProduct = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    editing: false,
  });
};
exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  // const product = new Product(null, title, imageUrl, description, price);

  //sequelize model
  // Product.create({
  //   title: title,
  //   price: price,
  //   imageUrl: imageUrl,
  //   description: description,
  // })
  //   .then((result) => {
  //     // console.log(result);
  //     res.redirect("/admin/products");
  //   })
  //   .catch((err) => console.log(err));

  // authentication
  // req.user
  //   .createProduct({
  //     title: title,
  //     price: price,
  //     imageUrl: imageUrl,
  //     description: description,
  //   })
  //   .then((result) => {
  //     console.log(result);
  //     res.redirect("/admin/products");
  //   })
  //   .catch((err) => console.log(err));

  // product.save();
  // res.redirect("/");

  // product
  //   .save()
  //   .then(() => {
  //     res.redirect("/");
  //   })
  //   .catch((err) => console.log(err));

  // mongodb
  const product = new Product(title, price, description, imageUrl,null, req.user._id);

  product
    .save()
    .then((result) => {
      console.log(result);
      res.redirect("/admin/products");
    })
    .catch((err) => console.log(err));
};

exports.getProducts = (req, res, next) => {
  // Product.fetchAll((products) => {
  //   res.render("admin/products", {
  //     prods: products,
  //     path: "/admin/products",
  //   });
  // });

  // sequelize method
  // Product.findAll()
  //   .then((products) => {
  //     res.render("admin/products", {
  //       prods: products,
  //       path: "/admin/products",
  //     });
  //   })
  //   .catch((err) => console.log(err));

  // authentication
  //   req.user
  //     .getProducts()
  //     .then((products) => {
  //       res.render("admin/products", {
  //         prods: products,
  //         path: "/admin/products",
  //       });
  //     })
  //     .catch((err) => console.log(err));

  // mongodb connection
  Product.fetchAll()
    .then((products) => {
      res.render("admin/products", {
        prods: products,
        path: "/admin/products",
      });
    })
    .catch((err) => console.log(err));
};

// exports.getEditProduct = (req, res, next) => {
//   const editMode = req.query.edit;
//   if(!editMode){
//     return res.redirect('/')
//   }
//   const prodId = req.params.productId;
//   Product.findById(prodId, (product) => {
//     if (!product) {
//       return res.redirect("/");
//     }
//     res.render("/admin/edit-product", {
//       pageTitle: "edit Product",
//       path: "/admin/edit-product",
//       editing: editMode,
//       product: product,
//     });
//   });
// };

exports.getEditProduct = (req, res, next) => {
  //   const editMode = req.query.edit;
  //   if (!editMode) {
  //     return res.redirect("/");
  //   }
  //   const prodId = req.params.productId;
  // Product.findById(prodId, (product) => {
  //   if (!product) {
  //     return res.redirect("/");
  //   }
  //   res.render("admin/edit-product", {
  //     pageTitle: "Edit Product",
  //     path: "/admin/edit-product",
  //     editing: editMode,
  //     product: product,
  //   });
  // });

  // sequelize method

  // Product.findByPk(prodId)
  //   .then((product) => {
  //     if (!product) {
  //       return res.redirect("/");
  //     }
  //     res.render("admin/edit-product", {
  //       pageTitle: "Edit Product",
  //       path: "/admin/edit-product",
  //       editing: editMode,
  //       product: product,
  //     });
  //   })
  //   .catch((err) => console.log(err));

  //use authentication

  //   req.user
  //     .getProducts({ where: { id: prodId } })
  //     .then((products) => {
  //       const product = products[0];
  //       if (!product) {
  //         return res.redirect("/");
  //       }
  //       res.render("admin/edit-product", {
  //         pageTitle: "Edit Product",
  //         path: "/admin/edit-product",
  //         editing: editMode,
  //         product: product,
  //       });
  //     })
  //     .catch((err) => console.log(err));

  //mongodb connection
  const editMode = req.query.edit;
  if (!editMode) {
    return res.redirect("/");
  }
  const prodId = req.params.productId;
  Product.findById(prodId)
    .then((product) => {
      if (!product) {
        return res.redirect("/");
      }
      res.render("admin/edit-product", {
        pageTitle: "Edit Product",
        path: "/admin/edit-product",
        editing: editMode,
        product: product,
      });
    })
    .catch((err) => console.log(err));
};

exports.postEditProduct = (req, res, next) => {
  //   const prodId = req.body.productId;
  //   const updatedTitle = req.body.title;
  //   const updatedPrice = req.body.price;
  //   const updatedImageUrl = req.body.imageUrl;
  //   const updatedDescription = req.body.description;

  // const updateProduct = new Product(
  //   prodId,
  //   updatedTitle,
  //   updatedImageUrl,
  //   updatedDescription,
  //   updatedPrice
  // );

  // sequelize model

  //   Product.findByPk(prodId)
  //     .then((product) => {
  //       product.title = updatedTitle;
  //       product.price = updatedPrice;
  //       product.description = updatedDescription;
  //       product.imageUrl = updatedImageUrl;
  //       return product.save();
  //     })
  //     .then((result) => {
  //       console.log(result);
  //       res.redirect("/admin/products");
  //     })
  //     .catch((err) => console.log(err));

  //   // updateProduct.save();

  // mongodb connection

  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDescription = req.body.description;
  const product = new Product(
    updatedTitle,
    updatedPrice,
    updatedDescription,
    updatedImageUrl,
    prodId
  );
  product
    .save()
    .then((product) => {
      console.log(product);
      res.redirect("/admin/products");
    })
    .catch((err) => console.log(err));
};

exports.postDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  //   // Product.deleteById(prodId);

  //   // sequelize model
  //   Product.findByPk(prodId)
  //     .then((product) => {
  //       return product.destroy();
  //     })
  //     .then((result) => {
  //       console.log(result);
  //       res.redirect("/admin/products");
  //     })
  //     .catch((err) => console.log(err));

  // mongodb connection
  Product.deleteById(prodId)
    .then(() => {
      res.redirect('/admin/products')
    })
    .catch((err) => console.log(err));
};
